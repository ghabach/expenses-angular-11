import { Component, OnInit } from '@angular/core';
import { Expense } from '../expense.model';
import { ExpensesService } from '../expenses.service';
import { User } from '../user.model';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css']
})
export class ExpensesComponent implements OnInit {

  user: User;
  expenses: Expense[];
  totalAmount: number;
  lastUpdated: any;

  constructor(public expensesService: ExpensesService) { }

  ngOnInit(): void {
    this.user = this.expensesService.getUser();
    this.expensesService.getExpenses()
    .subscribe((expenses)=>{
      this.expenses = expenses as Expense[];
      // for(let e of expenses as Expense[]){
      //   let expense = new Expense(e.date)
      //   this.expenses.push()  e.amount;
      // }
    });
    this.expensesService.getTotal()
    .subscribe((total)=>{
      this.totalAmount = total["amount"];
      this.lastUpdated = total["lastUpdated"];
    });
  }

}
